# Vodafone QuickCheck Scraper

This will scrape <http://quickcheck.vodafone.de/>, which is a simple web page by Vodafone Germany that shows a mobile data usage summary.

You may call this tool on the command line to get structured JSON data, or import it as a Python module and work with the `get_stats()` function.

When calling this tool on the command line (as opposed to embedding it in your own code), the JSON output will be prettified (as shown below) if standard output is a terminal; otherwise, it will be minified (all whitespace removed).
To force minified output, set the environment variable `JSON_COMPACT` to an empty string.
To force pretty output, set it to a non-empty string.

## Example output

```json
{
  "now": "2021-02-08 05:55:22.347819",
  "interval": {
    "this_start": "2021-01-26",
    "this_end": "2021-02-25",
    "next_start": "2021-02-26",
    "total_days": 31,
    "passed_days": 13,
    "left_days": 18,
    "passed_percent": 42,
    "left_percent": 58
  },
  "data": {
    "total_mb": 51200,
    "used_mb": 10701,
    "remaining_mb": 40499,
    "used_percent": 20.9,
    "remaining_percent": 79.1,
    "daily_mb": 823,
    "projected_mb": 25515,
    "projected_percent": 49.8
  }
}
```

Some remarks regarding these values:

* `interval` describes the billing interval.
  * `next_start` is calculated to be one month after `this_start`. `this_end` is one day before `next_start`.
  * `total_days` is the number of days between `this_start` and `next_start`.
  * `passed_days` is the number of days since `this_start`.
  * `left_days` is the number of days until `next_start`.
  * `passed_percent` and `left_percent` simply compare the respective `_days` value to `total_days`.
* `data` describes mobile data usage.
  * `total_mb`, `used_mb` and `remaining_mb` are what the Vodafone page tells us.
  * `used_percent` and `remaining_percent` compare the respective `_mb` value to `total_mb`.
  * `daily_mb` is dividing `total_mb` by `passed_days` (or `1` if `passed_days` is `0`).
  * `projected_mb` multiplies `daily_mb` by `left_days` and adds it to `used_mb`. This is an estimate of how much data you’ll likely use this month.
  * `projected_percent` compares `projected_mb` to `total_mb`. This can be more than `100` if it’s estimated that you’ll need more data than your plan allows.

If the page can’t be parsed correctly, not all of the values may be present.

## Requirements

This project requires [BeautifulSoup 4](https://www.crummy.com/software/BeautifulSoup/) and Python 3.

## Quirks

* This code is not timezone-aware. The reasoning is that you’re requesting the data from a German carrier from inside Germany.
* Additionally, the granularity is “days”. We should probably instead work with “seconds” granularity, but in order to do that, we first need to know whether the billing interval starts at midnight (and if yes, in which time zone).
* The Vodafone page only tells us when the current billing interval starts, not when it ends or when the next one starts. As far as I can see, the next one starts on the same day of the month, but I don’t know what happens if that month doesn’t have that many days. My billing interval always starts on the 26th; please report back if yours starts on the 28th or later.
* We only care about the billing interval and mobile data usage, not phone calls or SMS texts. This is simply because the author is using a data-only plan. Pull requests are welcome.
