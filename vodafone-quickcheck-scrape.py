#!/usr/bin/env python

import datetime
import json
from os import environ
import re
import sys
from urllib.request import urlopen

from bs4 import BeautifulSoup

def interval_start(body):
    expr = re.compile(r'^seit dem (\d{1,2})\.(\d{2})\.(\d{4})$')
    text = body.find(string=expr)
    if not text:
        return None
    match = expr.match(text)
    if not match:
        return None
    return datetime.date(int(match[3]), int(match[2]), int(match[1]))

def data_usage(body):
    expr = re.compile(r'^(\d+)\s*MB$')
    amounts = body.find_all(string=expr)
    if not len(amounts) == 3:
        return None
    return {
        'total': int(expr.match(amounts[0])[1]),
        'used': int(expr.match(amounts[1])[1]),
        'remaining': int(expr.match(amounts[2])[1]),
    }

def stats(body):
    now = datetime.datetime.now()
    today = now.date()
    result = {
        'now': str(now),
    }

    start = interval_start(body)
    if start:
        next_year = start.year+1 if start.month == 12 else start.year
        next_month = 1 if start.month == 12 else start.month+1
        # TODO: What if start.day is invalid in next_month?
        # Not sure whether Vodafone isn't limiting the days to be <29 though.
        next_start = start.replace(year=next_year, month=next_month)
        days_total = (next_start - start).days
        days_passed = (today - start).days
        days_left = (next_start - today).days
        result['interval'] = {
            'this_start': str(start),
            'this_end': str(next_start - datetime.timedelta(days=1)),
            'next_start': str(next_start),
            'total_days': days_total,
            'passed_days': days_passed,
            'left_days': days_left,
            'passed_percent': round(100 * days_passed / days_total),
            'left_percent': round(100 * days_left / days_total),
        }

    data = data_usage(body)
    if data:
        result['data'] = dict((k+'_mb', v) for k,v in data.items())
        result['data'].update({
            'used_percent': round(100 * data['used'] / data['total'], 1),
            'remaining_percent': round(100 * data['remaining'] / data['total'], 1),
        })
        if start:
            int_stats = result['interval']
            per_day = round(data['used'] / max(1, int_stats['passed_days']))
            projected = data['used'] + per_day * int_stats['left_days']
            result['data'].update({
                'daily_mb': per_day,
                'projected_mb': projected,
                'projected_percent': round(100 * projected / data['total'], 1),
            })

    return result

def get_stats(url='http://quickcheck.vodafone.de/'):
    with urlopen(url) as res:
        soup = BeautifulSoup(res, 'html.parser')
        body = soup.body
        return(stats(body))

if __name__ == '__main__':
    compact = environ.get("JSON_COMPACT", not sys.stdout.isatty())
    print(json.dumps(
        get_stats(),
        indent=None if compact else 2,
        separators=(",", ":") if compact else (", ", ": "),
    ))
